package com.smartsystems;

public final class MoreObjects {

  private final int attributeA;

  public MoreObjects(final int attributeA) {
    this.attributeA = attributeA;
  }

  public int getAttributeA() {
    return attributeA;
  }

  static class StaticInnerClass {
    private final int attributeB;

    public StaticInnerClass(final int attributeB) {
      this.attributeB = attributeB;
    }

    public int getAttributeB() {
      return attributeB;
    }
  }

  class NonStaticInnerClass {
    private final int attributeC;

    public NonStaticInnerClass(final int attributeC) {
      this.attributeC = attributeC;
    }

    public int getAttributeC() {
      return attributeC; //*
    }
  }

  public static void main(final String[] args) {
    final MoreObjects moreObjects = new MoreObjects(1);

    final StaticInnerClass staticInnerClass = new StaticInnerClass(2);

    final NonStaticInnerClass nonStaticInnerClass = moreObjects.new NonStaticInnerClass(3);

    System.out.printf("MoreObjects's attributeA = %d%n", moreObjects.getAttributeA());
    System.out.printf("StaticInnerClass's attributeB = %d%n", staticInnerClass.getAttributeB());
    System.out.printf("NonStaticInnerClass's attributeC = %d%n", nonStaticInnerClass.getAttributeC());
  }
}
