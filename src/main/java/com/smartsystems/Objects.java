package com.smartsystems;

public final class Objects {

  static class Person {

    static int howManyPeopleAreThere = 0;

    private final String firstName;

    private final String lastName;

    private final int age;

    public Person(final String firstName, final String lastName, final int age) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.age = age;
      howManyPeopleAreThere++;
    }

    public static int getHowManyPeopleAreThere() {
      return howManyPeopleAreThere;
    }

    public String getFirstName() {
      return firstName;
    }

    public String getLastName() {
      return lastName;
    }

    public int getAge() {
      return age;
    }

    public void jumpOnPlace() {
      if (age <= 5) {
        System.out.println("I can jump 10 cm high!");
      } else if (age <= 10) {
        System.out.println("I can jump 20 cm high!");
      } else if (age <= 20) {
        System.out.println("I can jump 40 cm high!");
      } else if (age <= 30) {
        System.out.println("I can jump 50 cm high!");
      } else if (age <= 40) {
        System.out.println("I can jump 40 cm high!");
      } else if (age <= 50) {
        System.out.println("I can jump 20 cm high");
      } else if (age <= 60) {
        System.out.println("I can jump 15 cm high");
      } else if (age > 60) {
        System.out.println("I can jump 10 cm high");
      }
    }

    public void jumpOver(final double height) {
      final boolean canIMakeIt = (age <= 5 && height < 10.0)
          || (age <= 10 && height <= 20.0)
          || (age <= 20 && height <= 40.0)
          || (age <= 30 && height <= 50.0)
          || (age <= 40 && height <= 40.0)
          || (age <= 50 && height <= 20.0)
          || (age <= 60 && height <= 15.0)
          || (age > 60 && height <= 10.0);

      if (canIMakeIt) {
        System.out.println("Easy cake!");
      } else {
        System.out.printf("There's no way I can jump over a %.2f cm high object!%n", height);
      }
    }
  }

  public static void main(final String[] args) {
    final Person person = new Person("Edinson", "Padrón", 31);

    System.out.println("First name: " + person.getFirstName());
    System.out.println("Last name: " + person.getLastName());
    System.out.println("Age: " + person.getAge());

    person.jumpOnPlace();
    person.jumpOver(50.5);

    System.out.println("-----------------------------------");

    final var anotherPerson = new Person("Laura", "Rodríguez", 29);

    System.out.println("First name: " + anotherPerson.getFirstName());
    System.out.println("Last name: " + anotherPerson.getLastName());
    System.out.println("Age: " + anotherPerson.getAge());

    anotherPerson.jumpOnPlace();
    anotherPerson.jumpOver(50.5);

    System.out.println("-----------------------------------");

    System.out.printf("There are %d people%n", Person.getHowManyPeopleAreThere());
  }
}
