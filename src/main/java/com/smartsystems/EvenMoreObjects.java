package com.smartsystems;

public final class EvenMoreObjects {

  static interface Shape {
    double area();
  }

  static class Circle implements Shape {
    private double radius;

    public Circle(final double radius) {
      this.radius = radius;
    }

    public double getRadius() {
      return radius;
    }

    public void setRadius(final double radius) {
      this.radius = radius;
    }

    @Override
    public double area() {
      return Math.PI * radius * radius;
    }
  }

  static class Rectangle implements Shape {
    private double length;

    private double width;

    public Rectangle(final double length, final double width) {
      this.length = length;
      this.width = width;
    }

    public double getLength() {
      return length;
    }

    public void setLength(final double length) {
      this.length = length;
    }

    public double getWidth() {
      return width;
    }

    public void setWidth(final double width) {
      this.width = width;
    }

    @Override
    public double area() {
      return length * width;
    }
  }

  static class GeometryGenious {

    public void calculateAreaOfShape(final Shape shape) {
      System.out.println("Calculating area of shape...");
      System.out.printf("The shape's area is %.2f%n", shape.area());
    }

    public void calculateAreaOfShape(final Circle circle) {
      System.out.println("Calculating area of circle...");
      System.out.printf("The circle's area is %.2f%n", circle.area());
    }

    public void calculateAreaOfShape(final Rectangle rectangle) {
      System.out.println("Calculating area of rectangle...");
      System.out.printf("The rectangle's area is %.2f%n", rectangle.area());
    }

    public void calculateAreaOfShape(final double radius) {
      System.out.printf("Calculating area of circle with radius %.2f...%n", radius);
      System.out.printf("The circle's area is %.2f%n", Math.PI * radius * radius);
    }

    public void calculateAreaOfShape(final double length, final double width) {
      System.out.printf("Calculating area of rectangle with length = %.2f and width = %.2f...%n", length, width);
      System.out.printf("The rectangle's area is %.2f%n", length * width);
    }
  }

  public static void main(final String[] args) {
    final GeometryGenious geometryGenious = new GeometryGenious();

    final Circle circle = new Circle(2);

    geometryGenious.calculateAreaOfShape(circle);

    System.out.println("------");

    final Rectangle rectangle = new Rectangle(2, 3);

    geometryGenious.calculateAreaOfShape(rectangle);

    System.out.println("------");

    geometryGenious.calculateAreaOfShape(2);

    System.out.println("------");

    geometryGenious.calculateAreaOfShape(2, 3);
  }
}
