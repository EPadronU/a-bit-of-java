package com.smartsystems;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class Generics {

  public static abstract class Chocolate { }

  public static class ChocolateNegro extends Chocolate { }

  public static class ChocolateNegroAl80 extends ChocolateNegro { }

  public static class ChocolateBlanco extends Chocolate { }

  public static abstract class PostreDeChocolate { }

  public static class TortaDeChocolate extends PostreDeChocolate { }

  public static class SufléDeChocolate extends PostreDeChocolate { }

  public static class FábricaDeTortasNegrasDeChocolate {

    private final List<ChocolateNegro> reservasDeChocolate = new ArrayList<>();

    // El parámetro `cargamentoDeChocolate` es el productor
    // Pues produce el chocolate que estamos consumiendo
    public void consumirChocolate(final List<? extends ChocolateNegro> cargamentoDeChocolate) {
      reservasDeChocolate.addAll(cargamentoDeChocolate);
    }

    // El parámetro `carrito` es el consumidor
    // Pues consume las tortas que estamos produciendo
    public void producirTortas(final List<? super TortaDeChocolate> carrito, final int cantidad) {
      for (int i = 0; i < cantidad; i++) {
        carrito.add(new TortaDeChocolate());
      }
    }
  }

  public static class FábricaDeSuflésDeChocolate {

    private final List<Chocolate> reservasDeChocolate = new ArrayList<>();

    // Producer Extends
    // El parámetro `cargamentoDeChocolate` es el productor
    // Pues produce el chocolate que estamos consumiendo
    public void consumirChocolate(final List<? extends Chocolate> cargamentoDeChocolate) {
      reservasDeChocolate.addAll(cargamentoDeChocolate);
    }

    // Consumer Super
    // El parámetro `carrito` es el consumidor
    // Pues consume las tortas que estamos produciendo
    public void producirSuflés(final List<? super SufléDeChocolate> carrito, final int cantidad) {
      for (int i = 0; i < cantidad; i++) {
        carrito.add(new SufléDeChocolate());
      }
    }
  }

  public static void main(final String[] args) {
    // Carrito donde entra toda clase de postres de chocolate
    final List<PostreDeChocolate> carrito = new ArrayList<>();

    final FábricaDeTortasNegrasDeChocolate tortasDelaNegra = new FábricaDeTortasNegrasDeChocolate();

    tortasDelaNegra.consumirChocolate(List.of(
        new ChocolateNegro(),
        // new ChocolateBlanco(), -- Solo trabajan con chocolate negro
        new ChocolateNegroAl80()));

    // Nos pedimos 3 tortas
    tortasDelaNegra.producirTortas(carrito, 3);

    final FábricaDeSuflésDeChocolate dulcesDeLaAbuela = new FábricaDeSuflésDeChocolate();

    dulcesDeLaAbuela.consumirChocolate(List.of(
        new ChocolateNegro(),
        new ChocolateBlanco(),
        new ChocolateNegroAl80()));

    // Nos pedimos 2 suflés
    dulcesDeLaAbuela.producirSuflés(carrito, 2);

    // Imprimimos el carrito
    System.out.println(carrito.stream()
        .map(Object::toString)
        .collect(Collectors.joining(",\n", "[", "]")));
  }
}
