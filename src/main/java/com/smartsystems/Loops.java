package com.smartsystems;

import java.util.Iterator;
import java.util.List;

public final class Loops {

  public static void main(final String[] args) {
    System.out.println("Traditional for");

    for (int number = 0; number < 10; number += 2) {
      System.out.printf("%d ", number);
    }

    System.out.printf("%n----%n");

    // --------------------------------------------------------------------

    System.out.println("Traditional for with array");

    final int[] myIntArray = { 10, 20, 30, 40 };

    for (int i = 0; i < myIntArray.length; i++) {
      System.out.printf("%d ", myIntArray[i]);
    }

    System.out.printf("%n----%n");

    // --------------------------------------------------------------------

    System.out.println("Enhanced for with array");

    for (final int myInt : myIntArray) {
      System.out.printf("%d ", myInt);
    }

    System.out.printf("%n----%n");

    // --------------------------------------------------------------------

    System.out.println("Enhanced for with collection");

    final List<Integer> myIntegerList = List.of(100, 200, 300);

    for (final Integer myInteger : myIntegerList) { // Anything that implements the Iterable interface
      System.out.printf("%d ", myInteger);
    }

    System.out.printf("%n----%n");

    // --------------------------------------------------------------------

    System.out.println("while");

    int number = 0;

    while (number < 10) {
      System.out.printf("%d ", number);

      number += 2;
    }

    System.out.printf("%n----%n");

    // --------------------------------------------------------------------

    System.out.println("A rough translation of the enhanced for with collection");

    final Iterator<Integer> iterator = myIntegerList.iterator();

    while (iterator.hasNext()) {
      System.out.printf("%d ", iterator.next());
    }

    System.out.printf("%n----%n");

    // --------------------------------------------------------------------

    System.out.println("do-while");

    int anotherNumber = 10;

    do {
      System.out.printf("%d ", anotherNumber);

      anotherNumber += 2;
    } while (anotherNumber < 10);

    System.out.printf("%n----%n");

    // --------------------------------------------------------------------

    System.out.println("break and continue");

    int myNumber = 0;

    while (true) {
      myNumber++;

      if (myNumber % 2 == 1) {
        continue;
      } else if (myNumber > 20) {
        break;
      }

      System.out.printf("%d ", myNumber);
    }

    System.out.printf("%n----%n");

    // --------------------------------------------------------------------

    System.out.println("Nesting and labels");

    boolean flag = false;

    dowhile: do {
      for (int i = 0; i < 4; i++) {
        System.out.printf("%b %d%n", flag, i);

        if (!flag && i >= 2) {
          flag = true;
          break;
        }

        if (flag && i == 3) {
          break dowhile;
        }
      }
    } while (true);
  }
}
