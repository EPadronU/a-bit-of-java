package com.smartsystems;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public final class LambdasI {

  public static final class MyComparator implements Comparator<Integer> {

    @Override
    public int compare(final Integer o1, final Integer o2) {
      return o1 - o2;
    }
  }

  public static void main(final String[] args) {
    final List<Integer> numbers = new ArrayList<>(List.of(4, 6, 9, 10, 0));

    System.out.println(String.format("Before sorting: %s", numbers));

    numbers.sort(new MyComparator());

    System.out.println(String.format("After sorting: %s", numbers));

    numbers.sort(new Comparator<Integer>() {

      @Override
      public int compare(final Integer o1, final Integer o2) {
        return o2 - o1;
      }
    });

    System.out.println(String.format("After reverse sorting: %s", numbers));

    numbers.sort((o1, o2) -> new Random().nextInt());

    System.out.println(String.format("After random sorting: %s", numbers));
  }
}
