package com.smartsystems;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Regex {

  public static class MyDate {

    private final String day;
    private final String month;
    private final String year;

    public MyDate(String day, String month, String year) {
      this.day = day;
      this.month = month;
      this.year = year;
    }

    @Override
    public String toString() {
      return "[MyDate]={day=" + day + ", month=" + month + ", year=" + year + "}";
    }
  }

  private static final Pattern datePattern = Pattern.compile(
      "(?<day>0[1-9]|[12][0-9]|3[01])/"
          + "(?<month>0[1-9]|1[012])/"
          + "(?<year>(?:19|2[0-9])[0-9]{2})");

  public static void main(String[] args) {
    final Matcher matcher = datePattern.matcher("01/07/2023 02/12/2023");

    List<MyDate> myDates = Stream.generate(() -> matcher.find())
        .takeWhile(b -> b)
        .map(ignore -> new MyDate(
            matcher.group("day"),
            matcher.group("month"),
            matcher.group("year")))
        .collect(Collectors.toList());

    System.out.println(myDates);
  }
}
