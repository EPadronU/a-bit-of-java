package com.smartsystems;

public final class Branches {

  public static void main(final String[] args) {
    final double motorcycleHorsePower = 14.5; // Nm

    System.out.println("if ------------------------");

    if (motorcycleHorsePower > 40.0) {
      System.out.println("That's a fine machine");
    }

    // --------------------------------------------------------------------

    System.out.println("if-else -------------------");

    if (motorcycleHorsePower > 40.0) {
      System.out.println("That's a fine machine");
    } else {
      System.out.println("At very least it's not a bicycle");
    }

    // --------------------------------------------------------------------

    System.out.println("if-else-if-else -----------");

    if (motorcycleHorsePower > 100.0) {
      System.out.println("That's a beast!");
    } else if (motorcycleHorsePower > 40.0) {
      System.out.println("That's a fine machine");
    } else {
      System.out.println("At very least it's not a bicycle");
    }

    // --------------------------------------------------------------------

    System.out.println("if-else-if-else (pt-mch)---");

    Object obj = 100.0;

    if (obj instanceof final Integer i) {
      System.out.printf("%d? Nice!%n", i);
    } else if (obj instanceof final Double d && d > 40.0) {
      System.out.printf("%,.2f? That's a fine machine %n", d);
    } else {
      System.out.println("🤷");
    }

    // --------------------------------------------------------------------

    System.out.println("Switch --------------------");

    String motorcycleBrand = "Honda";

    switch (motorcycleBrand) {
      case "Honda":
        System.out.println("Good quality and very reliable");
        break;
      case "Suzuki":
        System.out.println("Amazing machines for the price. Relatively cheap to maintain");
        break;
      case "Yamaha":
        System.out.println("Great quality. A little bit pricey to maintain");
        break;
      case "Kawasaki":
        System.out.println("You're rich, aren't you?");
        break;
      default:
        System.out.println("Don't know, don't care");
    }

    // --------------------------------------------------------------------

    System.out.println("Switch (cascading)---------");

    motorcycleBrand = "Suzuki";

    switch (motorcycleBrand) {
      case "Honda":
      case "Suzuki":
      case "Yamaha":
      case "Kawasaki":
        System.out.println("Japanese quality");
        break;
      default:
        System.out.println("Don't know, don't care");
    }

    // --------------------------------------------------------------------

    System.out.println("Switch (multiple)----------");

    motorcycleBrand = "Kawasaki";

    switch (motorcycleBrand) {
      case "Honda", "Suzuki", "Yamaha", "Kawasaki":
        System.out.println("Japanese quality");
        break;
      default:
        System.out.println("Don't know, don't care");
    }

    // --------------------------------------------------------------------

    System.out.println("Switch (exp-block)---------");

    motorcycleBrand = "AKT";

    int position = switch (motorcycleBrand) {
      case "Honda" -> {
        System.out.println("Good quality and very reliable");
        yield 2;
      }
      case "Suzuki" -> {
        System.out.println("Amazing machines for the price. Relatively cheap to maintain");
        yield 1;
      }
      case "Yamaha" -> {
        System.out.println("Great quality. A little bit pricey to maintain");
        yield 3;
      }
      case "Kawasaki" -> {
        System.out.println("You're rich, aren't you?");
        yield 4;
      }
      default -> {
        System.out.println("Don't know, don't care");
        yield -1;
      }
    };

    System.out.println("Brand's position: " + String.valueOf(position));

    // --------------------------------------------------------------------

    System.out.println("Switch (expression)--------");

    position = switch (motorcycleBrand) {
      case "Honda" ->  2;
      case "Suzuki" -> 1;
      case "Yamaha" -> 3;
      case "Kawasaki" -> 4;
      default -> {
        System.out.println("Don't know, don't care");
        yield -1;
      }
    };

    System.out.println("Brand's position: " + String.valueOf(position));

    // --------------------------------------------------------------------

    System.out.println("Switch (pttrn-mtch)--------");

    obj = "hello";

    switch (obj) {
      case final Integer i:
        System.out.printf("%,d%n", i);
        break;
      case final Float f:
        System.out.printf("%.2f%n", f);
        break;
      case final Double d:
        System.out.printf("%,.4f%n", d);
        break;
      case String s when s.length() > 0:
        System.out.printf("'%s'%n", s);
        break;
      default:
        System.out.printf("%s%n", obj);
    }
  }
}
