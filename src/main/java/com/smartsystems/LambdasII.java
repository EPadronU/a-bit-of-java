package com.smartsystems;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public final class LambdasII {

  public static class Torta {

    public Torta() {
    }
  }

  @FunctionalInterface
  public interface ProveedorDeTortas {

    Torta venderTorta();
  }

  static class TortasDeMaria implements ProveedorDeTortas {

    @Override
    public Torta venderTorta() {
      return new Torta();
    }
  }

  static class TortasDeLaNegra implements Supplier<Torta> {

    @Override
    public Torta get() {
      return new Torta();
    }
  }

  public static class Tienda {

    private final List<Torta> stockDeTortas = new ArrayList<>();

    public void abastecerseDeTortas(final Supplier<Torta> proveedorDeTortas, final int cantidad) {
      for (int i = 0; i < cantidad; i++) {
        stockDeTortas.add(proveedorDeTortas.get());
      }
    }
  }

  public static void main(final String[] args) {
    final Tienda tienda = new Tienda();

    final ProveedorDeTortas tiendaDeMaria = new TortasDeMaria();

    tienda.abastecerseDeTortas(Torta::new, 10);

    tienda.abastecerseDeTortas(new TortasDeLaNegra(), 5);
  }
}
