package com.smartsystems;

import java.util.Arrays;

public final class Variables {

  public static void primitiveTypes() {
    final boolean myBoolean = true; // or false

    final byte myByte = (byte) 0B11111111; // or 0b11111111

    final char myChar = 'a';

    final short myShort = (short) 0XFFFF; // or 0xFFFF

    final int myInt = 017777777777;

    final long myLong = (1L << 63) - 1L; // or (1l << 63) - 1l

    final float myFloat = 1.23456789F; // 1.23456789f

    final double myDouble = 1.23456789E10;

    System.out.printf("myBoolean =\t%b%n", myBoolean);
    System.out.println("-----");
    System.out.printf("myByte =\t%d(10)%n", myByte);
    System.out.printf("myByte =\t%d(10)(unsigned)%n", Byte.toUnsignedInt(myByte));
    System.out.printf("myByte =\t%o(8)%n", myByte);
    System.out.printf("myByte =\t%X(16)%n", myByte);
    System.out.println("-----");
    System.out.printf("myChar =\t%c%n", myChar);
    System.out.println("-----");
    System.out.printf("myShort =\t%d(10)%n", myShort);
    System.out.printf("myShort =\t%d(10)(unsigned)%n", Short.toUnsignedInt(myShort));
    System.out.printf("myShort =\t%o(8)%n", myShort);
    System.out.printf("myShort =\t%X(16)%n", myShort);
    System.out.println("-----");
    System.out.printf("myInt =\t\t%d(10)%n", myInt);
    System.out.printf("myInt =\t\t%s(10)(unsigned)%n", Integer.toUnsignedString(myInt));
    System.out.printf("myInt =\t\t%o(8)%n", myInt);
    System.out.printf("myInt =\t\t%X(16)%n", myInt);
    System.out.println("-----");
    System.out.printf("myLong =\t%d(10)%n", myLong);
    System.out.printf("myLong =\t%s(10)(unsigned)%n", Long.toUnsignedString(myLong));
    System.out.printf("myLong =\t%o(8)%n", myLong);
    System.out.printf("myLong =\t%X(16)%n", myLong);
    System.out.println("-----");
    System.out.printf("myFloat =\t%e%n", myFloat);
    System.out.printf("myFloat =\t%g%n", myFloat);
    System.out.printf("myFloat =\t%f%n", myFloat);
    System.out.println("-----");
    System.out.printf("myDouble =\t%e%n", myDouble);
    System.out.printf("myDouble =\t%g%n", myDouble);
    System.out.printf("myDouble =\t%f%n", myDouble);
  }

  public static void classTypes() {
    final String myString = "🔥Hello🔥";

    final String myBlockString = """
          Nice...
        ... Isn't it?
        """.strip();

    System.out.printf("myString = %s%n", myString);

    System.out.printf("myBlockString = %s%n", myBlockString);
  }

  public static void wrapperTypes() {
    final Boolean myBoolean = true; // or Boolean.valueOf(true) or Boolean.TRUE

    final Byte myByte = (byte) 0B11111111; // or Byte.valueof((byte) 0B11111111) or Byte.parseByte("-1", 2)

    final Character myChar = 'a'; // or Character.valueOf('a')

    final Short myShort = (short) 0XFFFF; // Short.parseShort("-1", 16)

    final Integer myInt = 017777777777; // Integer.parseInt("17777777777", 8)

    final Long myLong = (1L << 63) - 1L; // or Long.parseLong("7FFFFFFFFFFFFFFF", 16)

    final Float myFloat = 1.23456789F; // Float.valueOf(1.23456789F)

    final Double myDouble = 1.23456789E10; // Double.valueOf(123456789E10)

    System.out.printf("myBoolean =\t%b%n", myBoolean);
    System.out.printf("myByte =\t%s%n", myByte);
    System.out.printf("myChar =\t%c%n", myChar);
    System.out.printf("myShort =\t%X%n", myShort);
    System.out.printf("myInt =\t\t%o%n", myInt);
    System.out.printf("myLong =\t%X%n", myLong);
    System.out.printf("myFloat =\t%f%n", myFloat);
    System.out.printf("myDouble =\t%f%n", myDouble);
  }

  public static void arrays() {
    final int[] myIntArray = { 10, 20, 30, 40 };

    System.out.printf("myIntArray = %s%n", Arrays.toString(myIntArray));
    System.out.println("-----");

    final long[] myEmptyLongArray = new long[1];

    System.out.printf("myEmptyLongArray = %s%n", Arrays.toString(myEmptyLongArray));
    System.out.println("-----");

    final String[] myEmptyStringArray = new String[1];

    System.out.printf("myEmptyStringArray = %s%n", Arrays.toString(myEmptyStringArray));
    System.out.println("-----");

    final Boolean[] myBooleanArray = new Boolean[2];

    myBooleanArray[0] = Boolean.TRUE;
    myBooleanArray[1] = Boolean.FALSE;

    System.out.printf("myBooleanArray = %s%n", Arrays.toString(myBooleanArray));
    System.out.println("-----");

    final byte[] myTinyByteArray = new byte[0];

    System.out.printf("myTinyByteArray = %s%n", Arrays.toString(myTinyByteArray));
    System.out.println("-----");

    final int[][] myMultidimentionalIntArray = new int[2][];

    myMultidimentionalIntArray[0] = new int[3];
    myMultidimentionalIntArray[1] = new int[4];

    myMultidimentionalIntArray[0][0] = 1;
    myMultidimentionalIntArray[0][1] = 2;
    myMultidimentionalIntArray[0][2] = 3;

    myMultidimentionalIntArray[1][0] = 4;
    myMultidimentionalIntArray[1][1] = 5;
    myMultidimentionalIntArray[1][2] = 6;
    myMultidimentionalIntArray[1][3] = 7;
    System.out.printf("myMultidimentionalIntArray = %s%n", Arrays.toString(myMultidimentionalIntArray[0]));
    System.out.printf("myMultidimentionalIntArray = %s%n", Arrays.toString(myMultidimentionalIntArray[1]));
  }

  public static void main(final String[] args) {
    primitiveTypes();
    classTypes();
    wrapperTypes();
    arrays();
  }
}
