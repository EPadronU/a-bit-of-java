package com.smartsystems;

/*
 * ☑ Source file
 *  ☑ Main method
 * ☑ Variables
 *  ☑ Primitive types
 *    ☑ Literals
 *    ☑ Casting
 *  ☑ Class types
 *    ☑ Primitive wrapped types
 *  ☑ Arrays
 *    ☑ One-dimensional arrays
 *    ☑ Multi-dimensional arrays
 *  ☑ Read-only variables
 *  ☑ var keyword
 * ☑ Branches
 *  ☑ If
 *  ☑ If-else
 *  ☑ If-else-if-else
 *  ☑ Switch
 *  ☑ Switch expression
 *  ☑ Pattern matching
 * ☑ Loops
 *  ☑ Traditional (non-enhanced) for loop
 *  ☑ For-each (enhanced) loop
 *  ☑ While
 *  ☑ Do-while
 *  ☑ Nesting loops
 *    ☑ Labels
 * Objects
 *  ☑ Instant variable/attribute
 *  ☑ Methods/behavior
 *    ☑ Arguments and parameters
 *    ☑ Method signature
 *    ☑ Pass-by-copy
 *  ☑ Static members
 *    ☑ variables
 *    ☑ Methods
 *    ☑ Inner classes
 *  ☑ Encapsulation
 * ☒ The Standard library and its documentation
 * Inheritance and Polymorphism
 *  Overriding methods
 *  ☑ Overloading methods
 *  Abstract class
 *    Abstract methods
 */

public final class ABitOfJava {
  public static void main(final String[] args) {
    System.out.println("Hello, world!");
  }
}
