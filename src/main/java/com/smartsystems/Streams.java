package com.smartsystems;

import java.util.List;

public final class Streams {

  public static void main(String[] args) {
    final List<Integer> numbers = List.of(4, 9, 10, 15, -5, -15);

    numbers.stream()
      .filter(number -> number >= 0)
      .filter(number -> number % 3 == 0)
      .map(number -> number / 3)
      .forEach(number -> {
        for (int i = 0; i < number; i++) {
          System.out.print(number);
          System.out.print(" ");
        }
        System.out.println();
      });
  }
}
