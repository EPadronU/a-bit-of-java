package com.smartsystems;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.startsWith;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;

public final class ApiTests {

  @BeforeAll
  static void setUpRestAssured() {
    RestAssured.baseURI = "https://automationexercise.com";

    RestAssured.basePath = "/api";

    RestAssured.registerParser(ContentType.HTML.toString(), Parser.JSON);
  }

  @Test
  void aSimpleGet() {
    given()
        .header("Accept", ContentType.JSON)
        .when()
        .get("productsList")
        .then()
        .statusCode(200)
        .contentType(startsWith("text/html"))
        .header("server", "cloudflare")
        .time(lessThan(5_000L))
        .body("products.find( { it.id == 1 }).name", is("Blue Top"));
  }

  @Test
  void aSimplePost() {
    given()
        .formParams("search_product", "tshirt")
        .when()
        .post("searchProduct")
        .then()
        .statusCode(200)
        .body(matchesJsonSchemaInClasspath("schema.json"));
  }

  @Test
  void emptyResult() {
    given()
        .formParams("search_product", "kite")
        .when()
        .post("searchProduct")
        .then()
        .statusCode(200)
        .body("products", hasSize(0));
  }

  @Test
  void badPost() {
    when()
        .post("searchProduct")
        .then()
        .statusCode(200)
        .body("responseCode", is(400))
        .body("message", startsWith("Bad request"));
  }

  @Test
  void complexTest() {
    given()
        .formParam("name", "jiglesias")
        .formParam("email", "jiglesias@domain.com")
        .formParam("password", "1234")
        .formParam("title", "Mr")
        .formParam("birth_date", "30")
        .formParam("birth_month", "01")
        .formParam("birth_year", "1970")
        .formParam("firstname", "Julio")
        .formParam("lastname", "Iglesias")
        .formParam("company", "DMC")
        .formParam("address1", "Some")
        .formParam("address2", "Where")
        .formParam("country", "Colombia")
        .formParam("zipcode", "1234")
        .formParam("state", "Ant")
        .formParam("city", "Med")
        .formParam("mobile_number", "123456789")
        .when()
        .post("createAccount");

    final Pojo pojo = given()
        .queryParams("email", "jiglesias@domain.com")
        .when()
        .get("getUserDetailByEmail")
        .as(Pojo.class);

    System.out.println(pojo);

    given()
        .formParam("email", "jiglesias@domain.com")
        .formParam("password", "1234")
        .formParam("city", "Bog")
        .when()
        .put("updateAccount");

    given()
        .queryParams("email", "jiglesias@domain.com")
        .when()
        .get("getUserDetailByEmail")
        .then()
        .body("user.city", is("Bog"));

    given()
        .formParam("email", "jiglesias@domain.com")
        .formParam("password", "1234")
        .when()
        .delete("deleteAccount")
        .then()
        .statusCode(200)
        .body("responseCode", is(200));
  }

  private static final class Pojo {

    private int responseCode;

    private User user;

    public Pojo() {
    }

    public Pojo(int responseCode, User user) {
      this.responseCode = responseCode;
      this.user = user;
    }

    public int getResponseCode() {
      return responseCode;
    }

    public void setResponseCode(int responseCode) {
      this.responseCode = responseCode;
    }

    public User getUser() {
      return user;
    }

    public void setUser(User user) {
      this.user = user;
    }

    @Override
    public String toString() {
      return "[Pojo]={responseCode=" + responseCode + ", user=" + user + "}";
    }
  }

  private static final class User {

    private int id;
    private String name;
    private String email;
    private String title;
    private String birth_day;
    private String birth_month;
    private String birth_year;
    private String first_name;
    private String last_name;
    private String company;
    private String address1;
    private String address2;
    private String country;
    private String state;
    private String city;
    private String zipcode;

    public User() {
    }

    public User(
        int id,
        String name,
        String email,
        String title,
        String birth_day,
        String birth_month,
        String birth_year,
        String first_name,
        String last_name,
        String company,
        String address1,
        String address2,
        String country,
        String state,
        String city,
        String zipcode) {
      this.id = id;
      this.name = name;
      this.email = email;
      this.title = title;
      this.birth_day = birth_day;
      this.birth_month = birth_month;
      this.birth_year = birth_year;
      this.first_name = first_name;
      this.last_name = last_name;
      this.company = company;
      this.address1 = address1;
      this.address2 = address2;
      this.country = country;
      this.state = state;
      this.city = city;
      this.zipcode = zipcode;
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getBirth_day() {
      return birth_day;
    }

    public void setBirth_day(String birth_day) {
      this.birth_day = birth_day;
    }

    public String getBirth_month() {
      return birth_month;
    }

    public void setBirth_month(String birth_month) {
      this.birth_month = birth_month;
    }

    public String getBirth_year() {
      return birth_year;
    }

    public void setBirth_year(String birth_year) {
      this.birth_year = birth_year;
    }

    public String getFirst_name() {
      return first_name;
    }

    public void setFirst_name(String first_name) {
      this.first_name = first_name;
    }

    public String getLast_name() {
      return last_name;
    }

    public void setLast_name(String last_name) {
      this.last_name = last_name;
    }

    public String getCompany() {
      return company;
    }

    public void setCompany(String company) {
      this.company = company;
    }

    public String getAddress1() {
      return address1;
    }

    public void setAddress1(String address1) {
      this.address1 = address1;
    }

    public String getAddress2() {
      return address2;
    }

    public void setAddress2(String address2) {
      this.address2 = address2;
    }

    public String getCountry() {
      return country;
    }

    public void setCountry(String country) {
      this.country = country;
    }

    public String getState() {
      return state;
    }

    public void setState(String state) {
      this.state = state;
    }

    public String getCity() {
      return city;
    }

    public void setCity(String city) {
      this.city = city;
    }

    public String getZipcode() {
      return zipcode;
    }

    public void setZipcode(String zipcode) {
      this.zipcode = zipcode;
    }

    @Override
    public String toString() {
      return "[User]={id=" + id + ", name=" + name + ", email=" + email + ", title=" + title + ", birth_day="
          + birth_day + ", birth_month=" + birth_month + ", birth_year=" + birth_year + ", first_name=" + first_name
          + ", last_name=" + last_name + ", company=" + company + ", address1=" + address1 + ", address2=" + address2
          + ", country=" + country + ", state=" + state + ", city=" + city + ", zipcode=" + zipcode + "}";
    }
  }
}
